package com.mikesifr.findcat;

import android.content.res.AssetManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.mikesifr.findcat.FindCat;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class AndroidLauncher extends AndroidApplication {
	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
		initialize(new FindCat(getJson()), config);
	}


	private String getJson() {

		try {
			AssetManager.AssetInputStream fin = (AssetManager.AssetInputStream) this.getAssets().open("layers.json");
			BufferedReader reader = new BufferedReader(new InputStreamReader(fin));
			StringBuilder sb = new StringBuilder();
			String line;

			while ((line = reader.readLine()) != null) {
				sb.append(line).append("\n");
			}

			reader.close();
			fin.close();
			return sb.toString();

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}
}
