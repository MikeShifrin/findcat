package com.mikesifr.findcat;

import com.badlogic.gdx.graphics.Texture;

/**
 * Created by arina on 08.12.17.
 */

public class MyTexture {



    public String filename;
    public String type;
    public String transform;

    private String[] transforms;
    private Texture mTexture;

    private float height;
    private float width;
    private float positionX;
    private float positionY;


    public Texture getTexture() {
        return mTexture;
    }

    private void setTexture() {
        mTexture = new Texture(filename);
    }

    public void setOtherTexture(Texture texture) {
        mTexture = texture;
    }

    public float getWidth() {
        return width;
    }

    private void setWidth() {
        this.width = mTexture.getWidth() * Float.parseFloat(transforms[0]);
    }

    public float getHeight() {
        return height;
    }

    private void setHeight() {
        this.height = mTexture.getHeight() * Float.parseFloat(transforms[3]);
    }


    public float getPositionX() {
        return positionX;
    }

    private void setPositionX(float width) {
        this.positionX = ((width - mTexture.getWidth()) / 2) + Float.parseFloat(transforms[4]);
    }


    public float getPositionY() {
        return positionY;
    }

    private void setPositionY(float height) {
        this.positionY = ((height - mTexture.getHeight()) / 2) - Float.parseFloat(transforms[5]);
    }


    public void setValues(){
        String regex = "\\[|\\]";
        transforms = transform.replaceAll(regex, "").split(", ");

        setTexture();
        setWidth();
        setHeight();
    }

    public void setPositions(float width, float height){
        setPositionX(width);
        setPositionY(height);
    }

}
