package com.mikesifr.findcat;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Json;
import java.util.ArrayList;


public class FindCat extends ApplicationAdapter {

	private SpriteBatch batch;


	private ArrayList<MyTexture> mMyTextures;
	private OrthographicCamera camera;

	private float width, height;

	private String jsonAssets;

	public FindCat(String jsonAssets){
		super();
		this.jsonAssets = jsonAssets;
	}

	@Override
	public void create () {
		batch = new SpriteBatch();

		Json json = new Json();
		mMyTextures = json.fromJson(ArrayList.class, MyTexture.class, jsonAssets);

		for (MyTexture myTexture : mMyTextures){
			myTexture.setValues();
		}

		width = mMyTextures.get(0).getWidth();
		height = mMyTextures.get(0).getHeight();

		for (MyTexture myTexture : mMyTextures){
			myTexture.setPositions(width, height);
		}

		camera = new OrthographicCamera();
		camera.setToOrtho(false, width, height);
		batch.setProjectionMatrix(camera.combined);

	}

	@Override
	public void render () {
		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);


		camera.update();

		batch.begin();

		for(MyTexture myTexture : mMyTextures){
			batch.draw(myTexture.getTexture(),
					myTexture.getPositionX(),
					myTexture.getPositionY(),
					myTexture.getWidth(),
					myTexture.getHeight());
		}

		batch.end();
	}

	@Override
	public void dispose(){
		batch.dispose();
		for (MyTexture myTexture : mMyTextures){
			myTexture.getTexture().dispose();
		}
	}

	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
	}

}
